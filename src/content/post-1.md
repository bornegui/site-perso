---
title: "Chickpeas"
website: "https://chickpeas.tech"
draft: false
path: "/blog/chickpeas"
---

Allows you to instantly find the recipe you are looking for!
Specify a list of ingredients for the recipe, or get a random recipe.
Optionally, specify the type of dish you want (appetizer, main course or dessert), as well as a specific diet (vegetarian or vegan).

Chickpeas is made up of two applications:
- a Telegram bot (<a href="https://t.me/MyAwesomeRecipes_bot">Lien</a>)
- a responsive app (<a href="https://app.chickpeas.tech">Lien</a>)

The bot is written in Node, deployed on AWS Lambda, and deployment is handled by Seed via the Serverless framework.
Regarding the responsive app, the front-end is based on React/Apollo Client, and deployed on Netlify. The back-end is based on Node/GraphQL/Apollo Server, deployed on AWS Lambda, and the deployment is handled by Seed via the Serverless framework.
The landing page is a React/Gatsby project, deployed on Netlify.
