---
title: "SpaceX App"
website: "https://d15jtv7r1lh48b.cloudfront.net/"
draft: false
path: "/blog/spacex-app"
---

Display of the various launches made by SpaceX, as well as the various rockets and launchpads used.
The front-end is based on React/Apollo Client, deployed on AWS S3/CloudFront, and deployment is handled by GitLab CI/CD.
The back-end is based on Node/GraphQL/Apollo Server, deployed on AWS Lambda, and the deployment is handled by Seed through the Serverless framework.
The back-end requests a REST API (not developed by me) to retrieve all the data relating to SpaceX.


