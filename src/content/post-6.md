---
title: "Marketplace"
draft: false
path: "/blog/marketplace"
---

This project aims to connect buyers and sellers.
Single-repo project (with Lerna) broken down into a front-end based on React/Next.js and a back-end based on Node/Express/Sequelize/PostgreSQL.
The project is based on a Prestashop instance deployed on AWS, the resources were created via Terraform.


