---
title: "App Tradeshift"
draft: false
path: "/blog/tradeshift-app"
---

Tradeshift is a Procure-to-Pay solution allowing employees to have their expense validated by their superiors.
The application aims to be integrated into the catalog of Tradeshift applications and to provide real-time validation of expense reports.
The front-end is based on React/Tradeshift UI and the back-end on Node/Koa.


