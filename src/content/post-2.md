---
title: "Shopping list"
website: "https://shopping.chickpeas.tech"
draft: false
path: "/blog/shopping-app"
---

Shopping list with authentication accessible from a browser (responsive).

The project is made up of 2 microservices accessible behind a gateway.
A microservice is responsible for creating and authenticating users through JWT.
Another microservice contains logic specific to the shopping list.
These two microservices are based on Node/Express/Sequelize/PostgreSQL.

The gateway is based on express-gateway (Node/Express). It is the entry point in front of the 2 microservices: an unauthenticated user will not be able to access the shopping list microservice. He must first authenticate himself.

The gateway and the 2 microservices are deployed via Docker on an AWS EC2.

The front-end is based on Nuxt.js/Vue.js, and is deployed on Netlify.
Used store and components, as well as an auth middleware to restrict access to pages if the user is not identified.

V2: Used Strapi for authentication and business logic.
