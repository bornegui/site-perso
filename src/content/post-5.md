---
title: "GraphQL Gateway"
draft: false
path: "/blog/gateway"
---

This project is broken down into 3 sub-projects. 2 with Spring Boot and 1 with Node/GraphQL/Apollo Server which acts as a gateway and requests the right service according to the data to be requested.
All projects are Dockerized and the 2 Spring Boot services are behind an Nginx reverse-proxy.


